# FSE-Trabalho 2 - 2021-2_AlvaroGuimaraes



## Nome
Trabalho 2 de Fundamentos de Sistemas Embarcados (2021/2)

## Descrição
Projeto que simula um sistema de automação predial.

Essa simulação ocorre em um prédio de 2 andares, onde esse sistema deve ser responsável por monitorar e acionar sensores e dispositivos.

Projeto referente ao Trabalho 2 da matérias "Fundamentos de Sistemas Embarcados" da Universidade de Brasília.

O enunciado deste trabalho pode ser encontrado em https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-2-2021-2

## Processo de compilação
O sistema possui 2 módulos: um servidor central e servidor(es) distribuído(s).

Para utilizar o sistema, primeiro deve-se entrar na pasta "servidorCentral" e executar o comando
```
make
```

Com a compilação ocorrendo corretamente, basta executar o comando
```
./servCentr
```

Com isso, o servidor central estará rodando.

Para executar os servidores distribuídos, deve-se entrar na pasta "servidorDistribuido" e executar o comando
```
make
```

Com a compilação ocorrendo corretamente, basta chamar o executável passando um dos arquivos de configuração disponíveis como parâmetro da seguinte forma
```
./servDist <arquivoConfiguracao>
```

O servidor central deve ser executado primeiro para que o programa funcione corretamente.

## Uso
O servidor distribuído estará imprimindo as ações que estão ocorrendo entre o servidor distribuído e o servidor central.

Com ambos os programas executando, o servidor central mostrará uma interface para que o usuário possa tanto monitorar algumas informações como temperatura e umidade além de poder interagir com alguns dispositivos.

A interface estará constantemente mostrando para o usuário quais as ações ele pode realizar no sistema.

Para encerrar o sistema, o atalho Ctrl + C deve ser pressionado.

## Problemas conhecidos
Em alguns momentos, a interface construída com a biblioteca ncurses trava, exibindo alguns caracteres em posições inadequadas.

Há momentos em que o servidor distribuído demora um pouco para responder uma requisição do servidor central, quando isso ocorre, a interface leva um tempo para responder.

Para executar o programa, deve-se zerar os contadores na dashboard, senão o programa pode apontar uma quantidade incorreta de pessoas.