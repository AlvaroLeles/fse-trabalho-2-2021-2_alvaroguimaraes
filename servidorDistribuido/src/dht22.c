#include "dht22.h"

float temperatura;
float umidade;

float getTemperatura()
{
    return temperatura;
}

float getUmidade()
{
    return umidade;
}

int leDHT(int porta)
{
    int pinDHT = porta == 10143 ? 20 : 21;

    int dados[5] = {0, 0, 0, 0, 0};
    int tempo = 0;
    int ultimoEstado = HIGH;
    int aux = 0;

    defineOutput(pinDHT);

    escreveGPIO(pinDHT, LOW);
    delay(18);

    defineInput(pinDHT, "");

    while(leGPIO(pinDHT) == HIGH)
    {
        usleep(1);
    }

    for (int i = 0; i < MAXTIMINGS; i++)
    {
        tempo = 0;
        while(leGPIO(pinDHT) == ultimoEstado)
        {
            tempo++;
            if(tempo == 1000)
                break;
        }

        ultimoEstado = leGPIO(pinDHT);
        if (tempo == 1000)
            break;

        if (i > 3 && i % 2 == 0)
        {
            dados[aux / 8] <<= 1;

            if (tempo > 200)
                dados[aux / 8] |= 1;
            aux++;
        }
    }

    if (dados[4] == ((dados[0] + dados[1] + dados[2] + dados[3]) & 0xFF))
    {
        umidade = (dados[0] * 256 + dados[1]) / 10.0f;
        temperatura = ((dados[2] & 0x7F) * 256 + dados[3]) / 10.0f;

        if (dados[2] & 0x80)
            temperatura *= -1.0f;
            
        return 1;
    }

    return -1;
}