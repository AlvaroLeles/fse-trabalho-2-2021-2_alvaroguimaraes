#include "cliente.h"

int socketCliente;
pthread_t threadCliente;

void enviaArquivo(FILE* arquivo)
{
    char buffer[MAXSIZE] = {0};
    int n;

    while((n = fread(buffer, 1, sizeof(buffer), arquivo)) > 0)
    {
        send(socketCliente, buffer, n, 0);
        memset((char *)&buffer, 0, sizeof(buffer));
    }

    char final[3];
    final[0] = '#';
    final[1] = '@';
    final[2] = 0;

    send(socketCliente, final, 3, 0);
}

void iniciaCliente(char* ipServidor, int porta)
{
    struct sockaddr_in enderecoServ;

    socketCliente = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    if(socketCliente < 0)
    {
        printf("Erro ao tentar realizar comunicação socket\n");
        exit(1);
    }

    memset((char *)&enderecoServ, 0, sizeof(enderecoServ));
    enderecoServ.sin_family = AF_INET;
    enderecoServ.sin_addr.s_addr = inet_addr(ipServidor);
    enderecoServ.sin_port = htons(porta);

    if (connect(socketCliente, (struct sockaddr*)&enderecoServ, sizeof(enderecoServ)) < 0)
    {
        printf("Erro na conexão\n");
        exit(1);
    }
}

void fechaCliente()
{
    close(socketCliente);
}

void mandaDecimal(int tipo, float valor)
{
    send(socketCliente, &tipo, sizeof(tipo), 0);
    send(socketCliente, &valor, sizeof(valor), 0);
}

void mandaInt(int tipo, int valor)
{
    send(socketCliente, &tipo, sizeof(tipo), 0);
    send(socketCliente, &valor, sizeof(valor), 0);
}

void mandaEvento(int tipo)
{
    send(socketCliente, &tipo, sizeof(tipo), 0);
}

void* recebeComandos()
{
    while(1)
    {
        int tipo;
        int n;

        n = recv(socketCliente, &tipo, sizeof(tipo), 0);
        if (tipo == 1)
        {
            int gpio;
            n = recv(socketCliente, &gpio, sizeof(gpio), 0);
            printf("Enviando estado gpio %d\n", gpio);

            int estado = leGPIO(gpio);
            send(socketCliente, &estado, sizeof(estado), 0);
        }
        else if (tipo == 2)
        {
            int gpio;
            n = recv(socketCliente, &gpio, sizeof(gpio), 0);
            printf("Invertendo o estado da gpio %d\n", gpio);
            
            int retorno = toggleGPIO(gpio);
            int id = 7;
            send(socketCliente, &id, sizeof(id), 0);
            send(socketCliente, &retorno, sizeof(retorno), 0);
        }

        if (n == 0)
        {
            fechaThreadCliente();
            fechaCliente();
            exit(0);
        }
    }
}

void iniciaThreadCliente()
{
    pthread_create(&threadCliente, NULL, &recebeComandos, NULL);
}

void fechaThreadCliente()
{
    pthread_cancel(threadCliente);
    pthread_join(threadCliente, NULL);
}