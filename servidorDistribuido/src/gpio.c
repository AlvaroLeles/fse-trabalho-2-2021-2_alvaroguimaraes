#include "gpio.h"

int mapaPin[] =
{
    -1, -1, 8, 9, 7, 21, 22, 11, 10, 13, 12, 14, 26,
    23, -1, -1, 27, 0, 1, 24, 28, 29, 3, 4, 5, 2, 25, 2
};

int eventosAbertos[28];
int qtdEventos = 0;

struct configuracao configServidor;
struct timeval ultimaMudanca;

void cuidaPresenca()
{
    struct timeval agora;
    unsigned long diferenca;
    gettimeofday(&agora, NULL);
    diferenca = (agora.tv_sec * 1000000 + agora.tv_usec) - (ultimaMudanca.tv_sec * 1000000 + ultimaMudanca.tv_usec);

    if (diferenca > IGNORE_CHANGE_BELOW_USEC) {
        printf("Enviando presença\n");
        mandaEvento(3);
    }

    ultimaMudanca = agora;
}

void cuidaFumaca()
{
    struct timeval agora;
    unsigned long diferenca;
    gettimeofday(&agora, NULL);
    diferenca = (agora.tv_sec * 1000000 + agora.tv_usec) - (ultimaMudanca.tv_sec * 1000000 + ultimaMudanca.tv_usec);

    if (diferenca > IGNORE_CHANGE_BELOW_USEC) {
        printf("Enviando detecção fumaça\n");
        mandaEvento(4);
    }

    ultimaMudanca = agora;
}

void cuidaPessoaEntra()
{
    struct timeval agora;
    unsigned long diferenca;
    gettimeofday(&agora, NULL);
    diferenca = (agora.tv_sec * 1000000 + agora.tv_usec) - (ultimaMudanca.tv_sec * 1000000 + ultimaMudanca.tv_usec);

    if (diferenca > IGNORE_CHANGE_BELOW_USEC) {
        printf("Enviando pessoa entrou\n");
        mandaEvento(5);
    }

    ultimaMudanca = agora;
}

void cuidaPessoaSai()
{
    struct timeval agora;
    unsigned long diferenca;
    gettimeofday(&agora, NULL);
    diferenca = (agora.tv_sec * 1000000 + agora.tv_usec) - (ultimaMudanca.tv_sec * 1000000 + ultimaMudanca.tv_usec);

    if (diferenca > IGNORE_CHANGE_BELOW_USEC) {
        printf("Enviando pessoa saiu\n");
        mandaEvento(6);
    }

    ultimaMudanca = agora;
}

int convertePin(int pinGPIO)
{
    return mapaPin[pinGPIO];
}

void definePrioridadeMax()
{
    struct sched_param s;
    memset(&s, 0, sizeof(s));

    s.sched_priority = sched_get_priority_max(SCHED_FIFO);
    sched_setscheduler(0, SCHED_FIFO, &s);
}

void defineInput(int pinGPIO, char* tipo)
{
    pinMode(convertePin(pinGPIO), INPUT);

    if (strlen(tipo))
    {
        if (strcmp(tipo, "presenca") == 0 || strcmp(tipo, "janela") == 0 || strcmp(tipo, "porta") == 0)
            wiringPiISR(convertePin(pinGPIO), INT_EDGE_RISING, &cuidaPresenca);
        else if (strcmp(tipo, "fumaca") == 0)
            wiringPiISR(convertePin(pinGPIO), INT_EDGE_RISING, &cuidaFumaca);
        else if (strcmp(tipo, "contagem") == 0)
        {
            if (pinGPIO == 13 || pinGPIO == 2)
                wiringPiISR(convertePin(pinGPIO), INT_EDGE_RISING, &cuidaPessoaEntra);
            else if (pinGPIO == 19 || pinGPIO == 3)
                wiringPiISR(convertePin(pinGPIO), INT_EDGE_RISING, &cuidaPessoaSai);
        }
    }
}

void defineOutput(int pinGPIO)
{
    pinMode(convertePin(pinGPIO), OUTPUT);
}

int iniciaGPIO(struct configuracao config)
{
    configServidor = config;

    wiringPiSetup();

    gettimeofday(&ultimaMudanca, NULL);

    for (int i = 0; i < config.tamnhInputs; i++)
    {
        defineInput(config.inputs[i].gpio, config.inputs[i].type);
    }

    for (int i = 0; i < config.tamnhOutputs; i++)
    {
        defineOutput(config.outputs[i].gpio);
    }

    return 1;
}

int escreveGPIO(int pin, int status)
{
    int mapeado = 0;
    for(int i = 0; i < configServidor.tamnhOutputs; i++)
    {
        if (configServidor.outputs[i].gpio == pin)
        {
            mapeado = 1;
            break;
        }
    }

    if (!mapeado)
        return -1;

    digitalWrite(convertePin(pin), status);

    int nivelAtual = digitalRead(convertePin(pin));
    if (status == nivelAtual)
        return 1;
    
    return 0;
}

int leGPIO(int pin)
{
    return digitalRead(convertePin(pin));
}

int toggleGPIO(int pin)
{
    return escreveGPIO(pin, !leGPIO(pin));
}

int verificaInput(int pin)
{
    for (int i = 0; i < configServidor.tamnhInputs; i++)
    {
        if (configServidor.inputs[i].gpio == pin)
            return 1;
    }
    return 0;
}

int verificaOutput(int pin)
{
    for (int i = 0; i < configServidor.tamnhOutputs; i++)
    {
        if (configServidor.outputs[i].gpio == pin)
            return 1;
    }
    return 0;
}