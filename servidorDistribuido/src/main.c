#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include "gpio.h"
#include "dht22.h"
#include "jsonParse.h"
#include "cliente.h"

void cuidaSinal(int sinal);

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        printf("Arquivo de configuração JSON não informado\n");
        exit(1);
    }

    leArquivo(argv[1]);
    struct configuracao config = parseJson();

    if (!iniciaGPIO(config))
    {
        printf("Erro ao tentar inicializar GPIO\n");
        exit(1);
    }

    iniciaCliente(config.ip_servidor_central, config.porta_servidor_central);
    
    FILE* arquivo = fopen(argv[1], "r");
    if (arquivo == NULL)
    {
        printf("Erro ao ler arquivo de configuração\n");
        exit(1);
    }

    enviaArquivo(arquivo);
    fclose(arquivo);

    iniciaThreadCliente();

    while(1)
    {
        leDHT(config.porta_servidor_distribuido);
        float temperatura = getTemperatura();
        float umidade = getUmidade();

        printf("Enviando valores de temperatura e umidade\n");

        mandaDecimal(1, temperatura);
        mandaDecimal(2, umidade);
        
        sleep(1);
    }

    fechaCliente();
    fechaThreadCliente();
    fechaJson();

    return 0;
}

void cuidaSinal(int sinal)
{
    fechaCliente();
    fechaThreadCliente();
    fechaJson();
    exit(0);
}