#ifndef _CLIENTE_H_
#define _CLIENTE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "gpio.h"

#define MAXSIZE 256

void enviaArquivo(FILE* arquivo);
void iniciaCliente(char* ipServidor, int porta);
void fechaCliente();
void mandaDecimal(int tipo, float valor);
void mandaInt(int tipo, int valor);
void mandaEvento(int tipo);
void* recebeComandos();
void iniciaThreadCliente();
void fechaThreadCliente();

#endif