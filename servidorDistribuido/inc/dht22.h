#ifndef _DHT22_H_
#define _DHT22_H_

#include "gpio.h"

#define MAXTIMINGS 100

float getTemperatura();
float getUmidade();
int leDHT(int porta);

#endif