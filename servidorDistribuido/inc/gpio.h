#ifndef _GPIO_H_
#define _GPIO_H_

#include <wiringPi.h>
#include <sched.h>
#include <sys/mman.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>

#include "jsonParse.h"
#include "cliente.h"

#define ON  1
#define OFF 0

#define IGNORE_CHANGE_BELOW_USEC 200000

void cuidaPresenca();
void cuidaFumaca();
void cuidaPessoaEntra();
void cuidaPessoaSai();
int convertePin(int pinGPIO);
void definePrioridadeMax();
void defineInput(int pinGPIO, char* tipo);
void defineOutput(int pinGPIO);
int iniciaGPIO(struct configuracao config);
int escreveGPIO(int pin, int status);
int leGPIO(int pin);
int toggleGPIO(int pin);
int verificaInput(int pin);
int verificaOutput(int pin);

#endif