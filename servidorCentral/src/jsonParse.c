#include "jsonParse.h"

char* getString(cJSON *json, char* atributo)
{
    cJSON* conteudo = cJSON_GetObjectItemCaseSensitive(json, atributo);
    
    if(cJSON_IsString(conteudo) && (conteudo->valuestring != NULL))
        return conteudo->valuestring;
    
    printf("Erro ao ler atributo string do JSON\n");
    exit(1);
    return "";
}

int getInt(cJSON *json, char* atributo)
{
    cJSON* conteudo = cJSON_GetObjectItemCaseSensitive(json, atributo);

    if(cJSON_IsNumber(conteudo))
        return conteudo->valueint;

    printf("Erro ao ler atributo numérico do JSON\n");
    exit(1);
    return -1;
}

struct dispositivo* getArrayDispositivos(cJSON* json, char* atributo, unsigned int* tamanho)
{
    cJSON* dispositivos = NULL;
    cJSON* dispositivo = NULL;
    int idx = 0;

    struct dispositivo* arrayDispositivos;

    dispositivos = cJSON_GetObjectItemCaseSensitive(json, atributo);

    if (cJSON_IsArray(dispositivos))
    {
        *tamanho = cJSON_GetArraySize(dispositivos);
        arrayDispositivos = malloc(*tamanho * sizeof(struct dispositivo));

        cJSON_ArrayForEach(dispositivo, dispositivos)
        {
            struct dispositivo dispositivoVez;

            dispositivoVez.type = getString(dispositivo, "type");
            dispositivoVez.tag = getString(dispositivo, "tag");
            dispositivoVez.gpio = getInt(dispositivo, "gpio");

            arrayDispositivos[idx] = dispositivoVez;
            idx++;
        }
    }
    else
    {
        cJSON_Delete(json);
        printf("Erro ao ler array de dispositivos\n");
        exit(1);
    }

    return arrayDispositivos;
}

struct configuracao parseJson(FILE* arquivo)
{
    fseek(arquivo, 0, SEEK_END);
    int tamanhoArquivo = ftell(arquivo);
    fseek(arquivo, 0, SEEK_SET);

    char* buffer = malloc(tamanhoArquivo + 1);
    fread(buffer, 1, tamanhoArquivo, arquivo);
    fclose(arquivo);

    buffer[tamanhoArquivo] = 0;

    struct configuracao config;
    cJSON* configJson = cJSON_ParseWithLength(buffer, tamanhoArquivo);

    if (configJson == NULL)
    {
        const char* ptrErro = cJSON_GetErrorPtr();
        if (ptrErro != NULL)
            fprintf(stderr, "Erro ao ler JSON: %s\n", ptrErro);

        cJSON_Delete(configJson);
        printf("Erro ao ler JSON\n");
        exit(1);
    }
    
    config.ip_servidor_central = getString(configJson, "ip_servidor_central");
    config.porta_servidor_central = getInt(configJson, "porta_servidor_central");
    config.ip_servidor_distribuido = getString(configJson, "ip_servidor_distribuido");
    config.porta_servidor_distribuido = getInt(configJson, "porta_servidor_distribuido");
    config.nome = getString(configJson, "nome");

    config.outputs = getArrayDispositivos(configJson, "outputs", &config.tamnhOutputs);
    config.inputs = getArrayDispositivos(configJson, "inputs", &config.tamnhInputs);

    return config;
}