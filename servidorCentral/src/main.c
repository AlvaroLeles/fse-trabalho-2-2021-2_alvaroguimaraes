#include <pthread.h>
#include <signal.h>

#include "servidor.h"
#include "alarme.h"
#include "menu.h"

void cuidaSinal();

pthread_t thrServidor;

int main(int argc, char const *argv[])
{
    signal(SIGINT, cuidaSinal);

    int portaServidor = 10043;

    pthread_create(&thrServidor, NULL, &servidor, (void*)&portaServidor);

    iniciaMenu();

    infos();
    menu();

    cuidaSinal();

    return 0;
}

void cuidaSinal()
{
    fechaMenu();
    pthread_cancel(thrServidor);
    pthread_join(thrServidor, NULL);
    fechaSockets();
    exit(0);
}