#include "servidor.h"

int socketServidor;
struct sockaddr_in enderecoServ;
struct conexao listaConexoes[MAX_CONNECTIONS];
pthread_t threads_in[MAX_CONNECTIONS];
int tamanhoListaConexoes = 0;

struct conexao* getListaConexao()
{
    return listaConexoes;
}

int getTamanhoListaConexao()
{
    return tamanhoListaConexoes;
}

FILE* recebeArquivo(int socket)
{
    FILE* arquivo;
    char *nomeArquivo = "config.json";
    char buffer[SIZE] = {};
    int n;

    arquivo = fopen(nomeArquivo, "w+");
    int deveSair = 0;
    while((n = recv(socket, buffer, sizeof(buffer), 0)) > 0)
    {
        if (buffer[n - 3] == '#' && buffer[n - 2] == '@')
        {
            deveSair = 1;
            buffer[n - 3] = 0;
        }
        fputs(buffer, arquivo);
        memset((char *)&buffer, 0, sizeof(buffer));

        if (deveSair)
            break;
    }
    return arquivo;
}

int iniciaServidor(int porta)
{
    socketServidor = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (socketServidor < 0)
    {
        printf("Erro de socket do servidor\n");
        exit(1);
    }

    bzero((char *)&enderecoServ, sizeof(enderecoServ));
    enderecoServ.sin_family = AF_INET;
    enderecoServ.sin_addr.s_addr = inet_addr("192.168.0.53");
    enderecoServ.sin_port = htons(porta);

    if (bind(socketServidor, (struct sockaddr*)&enderecoServ, sizeof(enderecoServ)) < 0)
        return 0;

    if (listen(socketServidor, 10) < 0)
        return 0;

    return 1;
}

void fechaSockets()
{
    for (int i = 0; i < tamanhoListaConexoes; i++)
    {
        pthread_cancel(threads_in[i]);
        pthread_join(threads_in[i], NULL);
        close(listaConexoes[i].socket);
    }
    close(socketServidor);
}

void* servidor(void* arg)
{
    int porta = *((int*)arg);
    int resultado = iniciaServidor(porta);
    if (resultado < 0)
    {
        printf("Não foi possível abrir o servidor\n");
        exit(1);
    }

    while(1) {
        struct sockaddr_in enderecoCliente;
        socklen_t tamnhCliente = sizeof((struct sockaddr *) &enderecoCliente);
        int socketCliente = accept(socketServidor, (struct sockaddr*)&enderecoCliente, &tamnhCliente);
        
        if (socketCliente < 0)
            continue;

        FILE* arquivo = recebeArquivo(socketCliente);
        struct configuracao config = parseJson(arquivo); 
        struct conexao conexao;

        conexao.endereco = enderecoCliente;
        conexao.socket = socketCliente;
        conexao.config = config;

        pthread_create(&threads_in[tamanhoListaConexoes], NULL, (void*)escutaServDist, (void*)&conexao);

        listaConexoes[tamanhoListaConexoes++] = conexao;

    }
}

int solicitaEstado(int gpio, int socket)
{
    int request = 1;
    send(socket, &request, sizeof(request), 0);
    send(socket, &gpio, sizeof(gpio), 0);

    int estado;
    recv(socket, &estado, sizeof(estado), 0);
    return estado;
}

void realizaComando(int gpio)
{
    int idxConexao = getConexaoByOutput(gpio);
    if (idxConexao < 0) 
        return;

    int idxOutput = getOutputByGpio(listaConexoes[idxConexao].config, gpio);

    int request = 2;
    send(listaConexoes[idxConexao].socket, &request, sizeof(request), 0);
    send(listaConexoes[idxConexao].socket, &gpio, sizeof(gpio), 0);
    
    int tipo;
    int valor;
    recv(listaConexoes[idxConexao].socket, &tipo, sizeof(tipo), 0);
    recv(listaConexoes[idxConexao].socket, &valor, sizeof(valor), 0);

    if (valor)
        listaConexoes[idxConexao].config.outputs[idxOutput].estado = !listaConexoes[idxConexao].config.outputs[idxOutput].estado;
}

int encontraAspersor()
{
    for (int i = 0; i < tamanhoListaConexoes; i++)
    {
        for (int j = 0; j < listaConexoes[i].config.tamnhOutputs; j++)
        {
            if (strcmp(listaConexoes[i].config.outputs[j].type, "aspersor") == 0)
                return listaConexoes[i].config.outputs[j].gpio;
        }
    }
    return 0;
}

void attDispositivosTipo(char* tipo, int estado)
{
    for (int i = 0; i < tamanhoListaConexoes; i++)
    {
        for (int j = 0; j < listaConexoes[i].config.tamnhOutputs; j++)
        {
            if (strcmp(listaConexoes[i].config.outputs[j].type, tipo) == 0)
            {
                int estadoAtual = solicitaEstado(listaConexoes[i].config.outputs[j].gpio, listaConexoes[i].socket);

                if (estadoAtual != estado)
                    realizaComando(listaConexoes[i].config.outputs[j].gpio);
            }
        }
    }
}

int verificaTipo(char* tipo)
{
    for (int i = 0; i < tamanhoListaConexoes; i++)
    {
        for (int j = 0; j < listaConexoes[i].config.tamnhOutputs; j++)
        {
            if (strcmp(listaConexoes[i].config.outputs[j].type, tipo) == 0)
                return 1;
        }
    }
    return 0;
}

void removeConexao(int idx)
{
    close(listaConexoes[idx].socket);

    for (int i = idx; i < tamanhoListaConexoes; i++)
    {
        listaConexoes[i] = listaConexoes[i + 1];
    }
    tamanhoListaConexoes--;
}

void removeThread(int idx)
{
    pthread_cancel(threads_in[idx]);
    pthread_join(threads_in[idx], NULL);

    for (int i = idx; i < tamanhoListaConexoes + 1; i++)
    {
        threads_in[i] = threads_in[i + 1];
    }
    tamanhoListaConexoes--;
}

int comparaConexoes(struct conexao a, struct conexao b)
{
    int mesmoSocket = a.socket == b.socket;
    int mesmoIP = strcmp(inet_ntoa(a.endereco.sin_addr), inet_ntoa(b.endereco.sin_addr));
    int mesmaPorta = ntohs(a.endereco.sin_port) == ntohs(b.endereco.sin_port);
    return mesmoSocket && (mesmoIP == 0) && mesmaPorta;
}

void* escutaServDist(struct conexao* conexao)
{
    while(1) {
        int tipo;
        int n;
        n = recv(conexao->socket, &tipo, sizeof(tipo), 0);
        if (tipo == 1)
        {
            float valor;
            n = recv(conexao->socket, &valor, sizeof(valor), 0);
            attTemperatura(valor);
        }
        else if (tipo == 2)
        {
            float valor;
            n = recv(conexao->socket, &valor, sizeof(valor), 0);
            attUmidade(valor);
        }
        else if (tipo == 3)
        {
            int valor;
            n = recv(conexao->socket, &valor, sizeof(valor), 0);
            if (getAlarmePresenca())
                disparaAlarme();
        }
        else if (tipo == 4)
        {
            int valor;
            n = recv(conexao->socket, &valor, sizeof(valor), 0);
            if (getAlarmeFumaca())
            {
                disparaAlarme();
                int aspersor = encontraAspersor();
                if (aspersor)
                    realizaComando(aspersor);
            }
        }
        else if (tipo == 5)
        {
            int valor;
            n = recv(conexao->socket, &valor, sizeof(valor), 0);
            entraPessoa();
        }
        else if (tipo == 6)
        {
            int valor;
            n = recv(conexao->socket, &valor, sizeof(valor), 0);
            saiPessoa();
        }

        if (n == 0)
        {
            int idx;
            for (int i = 0; i < tamanhoListaConexoes; i++)
            {
                int igual = comparaConexoes(*conexao, listaConexoes[i]);
                if (igual)
                {
                    idx = i;
                    break;
                }
            }
            removeConexao(idx);
            removeThread(idx);
        }
    }
}

int getConexaoByOutput(int gpio)
{
    for (int i = 0; i < tamanhoListaConexoes; i++)
    {
        for (int j = 0; i < listaConexoes[i].config.tamnhOutputs; j++)
        {
            if (listaConexoes[i].config.outputs[j].gpio == gpio)
                return i;
        }
    }

    return -1;
}

int getOutputByGpio(struct configuracao config, int gpio)
{
    for (int i = 0; i < config.tamnhOutputs; i++)
    {
        if (config.outputs[i].gpio == gpio)
            return i;
    }

    return -1;
}