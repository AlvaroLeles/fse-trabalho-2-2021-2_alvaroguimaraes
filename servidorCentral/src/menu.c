#include "menu.h"

float temperatura = 0;
float umidade = 0;
int qtdPessoas = 0;

WINDOW* infosWindow;

void attTemperatura(float valor)
{
    temperatura = valor;
    mvwprintw(infosWindow, 3, 1, "Temperatura: %.1f", temperatura);
    wrefresh(infosWindow);
}

void attUmidade(float valor)
{
    umidade = valor;
    mvwprintw(infosWindow, 4, 1, "Umidade: %.1f", umidade);
    wrefresh(infosWindow);
}

void entraPessoa()
{
    qtdPessoas++;
    mvwprintw(infosWindow, 5, 1, "Quantidade de pessoas: %d", qtdPessoas);
    wrefresh(infosWindow);
}

void saiPessoa()
{
    qtdPessoas--;
    mvwprintw(infosWindow, 5, 1, "Quantidade de pessoas: %d", qtdPessoas);
    wrefresh(infosWindow);
}

void iniciaMenu()
{
    initscr();
    cbreak();
    echo();
    keypad(stdscr, 1);
    curs_set(0);
}

void fechaMenu()
{
    endwin();
}

int getOp(WINDOW * menuWin, char * escolhas[], int tamanho)
{
    int escolha;
    int destaque = 0;

    while (1)
    {
        for (int i = 0; i < tamanho; i++)
        {
            if (i == destaque)
                wattron(menuWin, A_REVERSE);

            mvwprintw(menuWin, i + 3, 1, escolhas[i]);
            wattroff(menuWin, A_REVERSE);
        }

        escolha = wgetch(menuWin);
        switch (escolha)
        {
            case KEY_UP:
                if (destaque > 0)
                    destaque--;
                break;
            case KEY_DOWN:
                if (destaque < tamanho - 1)
                    destaque++;
                break;
            default:
                break;
        }

        if (escolha == 10)
            return destaque;
    }
}

void attTitulo(WINDOW* menuWin, char* titulo)
{
    wattron(menuWin, A_BOLD);
    mvwprintw(menuWin, 1, ((COLS - 13) / 2) / 2, titulo);
    wattroff(menuWin, A_BOLD);
}

WINDOW* getMenu(int posicao, int qtd)
{
    int maxY;
    int maxX;
    getmaxyx(stdscr, maxY, maxX);

    WINDOW* menu = newwin(maxY, maxX / qtd, 0, posicao * (maxX / qtd));
    box(menu, 0, 0);

    wrefresh(menu);
    return menu;
}

void infos()
{
    infosWindow = getMenu(1, 2);
    attTitulo(infosWindow, "Infos:");

    mvwprintw(infosWindow, 3, 1, "Temperatura: %.1f", temperatura);
    mvwprintw(infosWindow, 4, 1, "Umidade: %.1f", umidade);
    mvwprintw(infosWindow, 5, 1, "Contador de Pessoas: %d", qtdPessoas);

    wrefresh(infosWindow);
}

void menu()
{
    WINDOW* menuWindow = getMenu(0, 2);
    keypad(menuWindow, true);

    while (1)
    {
        attWindow(menuWindow, "Tela Principal");
        char* opcoes[] = {"Interagir com um dispositivo", "Ligar alarme de presença", "Ligar alarme de fumaça"};

        if (getAlarmePresenca())
            opcoes[1] = "Desligar alarme de presença";
        
        if (getAlarmeFumaca())
            opcoes[2] = "Desligar alarme de fumaça";

        int entrada = getOp(menuWindow, opcoes, 3);

        if (entrada == 0)
        {
            struct conexao* listaConexao = getListaConexao();
            int tamanhoListaConexao = getTamanhoListaConexao();

            int qtdOpcoes = 0;

            for (int i = 0; i < tamanhoListaConexao; i++)
            {
                qtdOpcoes += listaConexao[i].config.tamnhOutputs;
            }
            
            char** nomesOpcoes = malloc(qtdOpcoes * sizeof(*nomesOpcoes));
            int* gpiosOpcoes = malloc(qtdOpcoes * sizeof(int));
            int* estadosOpcoes = malloc(qtdOpcoes * sizeof(int));

            int idx = 0;

            char** opcoesFinais = malloc((qtdOpcoes + 5) * sizeof(*opcoesFinais));

            for(int i = 0; i < tamanhoListaConexao; i++)
            {
                for (int j = 0; j < listaConexao[i].config.tamnhOutputs; j++)
                {
                    *(nomesOpcoes + idx) = listaConexao[i].config.outputs[j].tag;
                    gpiosOpcoes[idx] = listaConexao[i].config.outputs[j].gpio;

                    estadosOpcoes[idx] = solicitaEstado(listaConexao[i].config.outputs[j].gpio, listaConexao[i].socket);

                    char* resultado = malloc(9 + strlen(listaConexao[i].config.outputs[j].tag));

                    if (estadosOpcoes[idx])
                        strcpy(resultado, "Desligar ");
                    else
                        strcpy(resultado, "Ligar ");

                    strcat(resultado, listaConexao[i].config.outputs[j].tag);

                    *(opcoesFinais + idx) = resultado;

                    idx++;
                }
            }

            int tamanhoOpcoesExtras = 0;

            if (verificaTipo("lampada"))
            {
                opcoesFinais[qtdOpcoes + tamanhoOpcoesExtras] = "Ligar todas as lampadas";
                tamanhoOpcoesExtras++;
                opcoesFinais[qtdOpcoes + tamanhoOpcoesExtras] = "Desligar todas as lampadas";
                tamanhoOpcoesExtras++;
            }
            if (verificaTipo("ar-condicionado"))
            {
                opcoesFinais[qtdOpcoes + tamanhoOpcoesExtras] = "Ligar todos os ar-condicionados";
                tamanhoOpcoesExtras++;
                opcoesFinais[qtdOpcoes + tamanhoOpcoesExtras] = "Desligar todos os ar-condicionados";
                tamanhoOpcoesExtras++;
            }
            opcoesFinais[qtdOpcoes + tamanhoOpcoesExtras] = "Voltar";
            tamanhoOpcoesExtras++;

            werase(menuWindow);

            attWindow(menuWindow, "Menu");

            int entrada = getOp(menuWindow, opcoesFinais, qtdOpcoes + tamanhoOpcoesExtras);

            if (strcmp(opcoesFinais[entrada], "Ligar todas as lampadas") == 0)
                attDispositivosTipo("lampada", 1);
            else if (strcmp(opcoesFinais[entrada], "Desligar todas as lampadas") == 0)
                attDispositivosTipo("lampada", 0);
            else if (strcmp(opcoesFinais[entrada], "Ligar todos os ar-condicionados") == 0)
                attDispositivosTipo("ar-condicionado", 1);
            else if (strcmp(opcoesFinais[entrada], "Desligar todos os ar-condicionados") == 0)
                attDispositivosTipo("ar-condicionado", 0);
            else if (entrada >= 0 && entrada < qtdOpcoes)
                realizaComando(gpiosOpcoes[entrada]);

            wclear(menuWindow);

            free(opcoesFinais);
            free(nomesOpcoes);
            free(gpiosOpcoes);
            free(estadosOpcoes);
        }
        else if (entrada == 1)
        {
            toggleAlarmePresenca();
            wclear(menuWindow);
        }
        else if (entrada == 2)
        {
            toggleAlarmeFumaca();
            wclear(menuWindow);
        }
    }
}

void attWindow(WINDOW* menu, char* titulo)
{
    box(menu, 0, 0);
    attTitulo(menu, titulo);

    wrefresh(menu);
}