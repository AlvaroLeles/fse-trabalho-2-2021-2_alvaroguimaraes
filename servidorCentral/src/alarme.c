#include "alarme.h"

int alarmePresenca = 0;
int alarmeFumaca = 0;

int getAlarmePresenca()
{
    return alarmePresenca;
}

int getAlarmeFumaca()
{
    return alarmeFumaca;
}

int toggleAlarmePresenca()
{
    alarmePresenca = !alarmePresenca;
    return alarmePresenca;
}

int toggleAlarmeFumaca()
{
    alarmeFumaca = !alarmeFumaca;
    return alarmeFumaca;
}

void disparaAlarme()
{
    system("omxplayer somAlarme.mp3");
}