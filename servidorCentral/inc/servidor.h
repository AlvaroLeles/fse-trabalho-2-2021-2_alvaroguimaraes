#ifndef _SERVIDOR_H_
#define _SERVIDOR_H_

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "jsonParse.h"
#include "alarme.h"
#include "menu.h"

#define SIZE 256
#define MAX_CONNECTIONS 100

struct conexao {
    int socket;
    struct sockaddr_in endereco;
    struct configuracao config;
};

struct content {
    int tipo;
    float valor;
};

struct conexao* getListaConexao();
int getTamanhoListaConexao();
FILE* recebeArquivo(int socket);
int iniciaServidor(int porta);
void fechaSockets();
void* servidor(void* arg);
int solicitaEstado(int gpio, int socket);
void realizaComando(int gpio);
int encontraAspersor();
void attDispositivosTipo(char* tipo, int estado);
int verificaTipo(char* tipo);
void removeConexao(int idx);
void removeThread(int idx);
int comparaConexoes(struct conexao a, struct conexao b);
void* escutaServDist(struct conexao* conexao);
int getConexaoByOutput(int gpio);
int getOutputByGpio(struct configuracao config, int gpio);

#endif