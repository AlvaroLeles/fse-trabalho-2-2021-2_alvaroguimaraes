#ifndef _JSON_PARSE_H_
#define _JSON_PARSE_H_

#include <stdio.h>
#include <stdlib.h>

#include "cJSON.h"

struct dispositivo {
    char* type;
    char* tag;
    int gpio;
    int estado;
};

struct configuracao {
    char* ip_servidor_central;
    int porta_servidor_central;
    char* ip_servidor_distribuido;
    int porta_servidor_distribuido;
    char* nome;
    unsigned int tamnhOutputs;
    struct dispositivo* outputs;
    unsigned int tamnhInputs;
    struct dispositivo* inputs;
};

char* getString(cJSON *json, char* atributo);
int getInt(cJSON *json, char* atributo);
struct dispositivo* getArrayDispositivos(cJSON* json, char* atributo, unsigned int* tamanho);
struct configuracao parseJson();

#endif