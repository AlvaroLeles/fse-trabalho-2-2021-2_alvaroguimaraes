#ifndef _ALARME_H_
#define _ALARME_H_

#include <stdio.h>
#include <stdlib.h>

int getAlarmePresenca();
int getAlarmeFumaca();
int toggleAlarmePresenca();
int toggleAlarmeFumaca();
void disparaAlarme();

#endif