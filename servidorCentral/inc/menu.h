#ifndef _MENU_H_
#define _MENU_H_

#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>

#include "alarme.h"
#include "servidor.h"

void attTemperatura(float valor);
void attUmidade(float valor);
void entraPessoa();
void saiPessoa();
void iniciaMenu();
void fechaMenu();
int getOp(WINDOW * menuWin, char * escolhas[], int tamanho);
void attTitulo(WINDOW* menuWin, char* titulo);
WINDOW* getMenu(int posicao, int qtd);
void infos();
void menu();
void attWindow(WINDOW* menu, char* titulo);

#endif